# 230225

[gitlab-ci-flow](https://gitlab.com/klovtsov/devops_includes/gitlab_ci_flows/230225/-/blob/main/gitlab-ci.yml?ref_type=v.1.0.0)

[gitlab-ci-includes](https://gitlab.com/klovtsov/devops_includes/gitlab_includes)

![cicd_config](./images/cicd_config.png)

## Gitlab CI/CD vars

| Type | Key | Description |
| --- | --- | --- |
| Variable | AWS_ACCESS_KEY_ID | [AWS access key](https://developer.hashicorp.com/terraform/language/settings/backends/s3#access_key) |
| Variable | AWS_SECRET_ACCESS_KEY | [AWS access key](https://developer.hashicorp.com/terraform/language/settings/backends/s3#secret_key) |
| File | ANSIBLE_PRIVATE_KEY | Private SSH ключ для деплоя |
| File | DEPLOY_PUBLIC_KEY | Public SSH ключ для деплоя |
| Variable | TF_VAR_ssh_key_path | $DEPLOY_PUBLIC_KEY |
| Variable | YC_CLOUD_ID | [cloud_id](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs#cloud_id) |
| Variable | YC_FOLDER_ID | [folder_id](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs#folder_id) |
| Variable | YC_SERVICE_ACCOUNT_KEY_FILE | [service_account_key_file](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs#service_account_key_file) |

### Terraform

#### Подготовка окружения

Предполагается, что установлен и настроен [yandex cloud cli](https://cloud.yandex.ru/docs/cli/quickstart).


Создание folder в yandex cloud

```
yc resource-manager folder create do230225
```

Меняем folder в конфиге cli

```
yc config set folder-name do230225
```

Создание сервис аккаунта для работы с terraform

```
yc iam service-account create --name sa230225
```

Добавление прав editor сервисному аккаунту

```
yc resource-manager cloud add-access-binding mycloud \
    --role editor \
    --subject userAccount:<USER_ID>
```

Создание статического ключа для сервис аккаунта

```
yc iam access-key create --service-account-name sa230225
```

Вывод ключа нужно сохранить в переменные:

key_id - AWS_ACCESS_KEY_ID
secret - AWS_SECRET_ACCESS_KEY

Создание авторизованного ключа для сервис аккаунта

```
yc iam key create --service-account-name my-robot --output key.json
```

Содержимое key.json нужно поместить в переменную YC_SERVICE_ACCOUNT_KEY_FILE

Создание S3 бакета для хранения terraform state

```
yc storage bucket create --name tf230225 --max-size 1048576
```

Создание пары ssh ключей

```
ssh-keygen -t rsa -q -f ./id_rsa -N ""
```
