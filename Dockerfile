# Build the application from source
FROM golang:1.20.6 AS build-stage

WORKDIR /app

COPY example/go.mod example/go.sum ./
RUN go mod download

COPY example/outyet/*.go ./

RUN CGO_ENABLED=0 GOOS=linux go build -o /outyet

# Deploy the application binary into a lean image
FROM gcr.io/distroless/base-debian11 AS build-release-stage

WORKDIR /

COPY --from=build-stage /outyet /outyet

EXPOSE 8080

USER nonroot:nonroot

ENTRYPOINT ["/outyet"]
