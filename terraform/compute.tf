resource "yandex_vpc_network" "vpc_network" {
  name = var.vpc_network_name
}

resource "yandex_vpc_subnet" "vms_subneta" {
  name           = var.vms_subneta_name
  zone           = var.vms_subneta_zone
  network_id     = yandex_vpc_network.vpc_network.id
  v4_cidr_blocks = var.vms_subneta_v4_cidr_blocks
}

data "yandex_compute_image" "app_image" {
  family = var.app_image_family
}

resource "yandex_compute_instance" "app" {
  count       = var.app_count
  hostname    = "${var.app_hostname}${format("%02d", count.index + 1)}"
  name        = "${var.app_hostname}${format("%02d", count.index + 1)}"
  platform_id = var.app_platform_id
  resources {
    cores         = var.app_cores
    memory        = var.app_memory
    core_fraction = var.app_core_fraction
  }
  scheduling_policy {
    preemptible = var.app_preemptible
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.app_image.id
      size     = var.app_disk_size
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vms_subneta.id
    nat       = true
  }

  metadata = {
    "ssh-keys" = "${var.ssh_key_user}:${file("${var.ssh_key_path}")}"
  }

  labels = var.app_labels
}
