variable "yc_zone" {
  type    = string
  default = "ru-central1-a"
}

variable "vpc_network_name" {
  type    = string
  default = "net_230225"
}

variable "vms_subneta_name" {
  type    = string
  default = "vmsubnet-a"
}

variable "vms_subneta_zone" {
  type    = string
  default = "ru-central1-a"
}

variable "vms_subneta_v4_cidr_blocks" {
  type    = list(string)
  default = ["192.168.10.0/24"]
}

variable "ssh_key_user" {
  type    = string
  default = "ubuntu"
}

variable "ssh_key_path" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}

variable "app_image_family" {
  type    = string
  default = "ubuntu-2204-lts"
}

variable "app_count" {
  type    = number
  default = 1
}

variable "app_hostname" {
  type    = string
  default = "app"
}

variable "app_platform_id" {
  type    = string
  default = "standard-v2"
}

variable "app_preemptible" {
  type    = bool
  default = true
}

variable "app_cores" {
  type    = number
  default = 2
}

variable "app_memory" {
  type    = number
  default = 2
}

variable "app_core_fraction" {
  type    = number
  default = 20
}

variable "app_disk_size" {
  type    = number
  default = 20
}

variable "app_labels" {
  type = map(string)
  default = {
    "ansible_group" = "app"
  }
}
