terraform {
  backend "s3" {
    key                         = "230225.tfstate"
    bucket                      = "tf230225"
    endpoint                    = "storage.yandexcloud.net"
    region                      = "ru-central1"
    skip_region_validation      = true
    skip_credentials_validation = true

    # AWS_ACCESS_KEY_ID
    # access_key = ""
    # AWS_SECRET_ACCESS_KEY
    # secret_key = ""
  }

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">=0.82.0"
    }
  }
}

provider "yandex" {
  # YC_TOKEN
  # token     = 
  # YC_CLOUD_ID
  # cloud_id = 
  # YC_FOLDER_ID
  # folder_id = 
  zone = var.yc_zone
}
