output "ip_app" {
  value = yandex_compute_instance.app[*].network_interface.0.ip_address
}

output "nat_ip_app" {
  value = yandex_compute_instance.app[*].network_interface.0.nat_ip_address
}

output "hostname_app" {
  value = yandex_compute_instance.app[*].fqdn
}
